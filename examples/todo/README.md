Issues:
* Autofocus does not work
* Double click for edit not focus on input
* Simi does not support router yet
* This example not store to persistent storage yet

The content of `static/style.scss` is from ["here"](https://github.com/tastejs/todomvc-app-css)

This **Todo example** app is license under the same license as [TodoMVC App Template](https://github.com/tastejs/todomvc-app-template)

CC-BY-4.0 © Simi-Project