use proc_macro2::{Ident, TokenStream};
use quote::quote;

#[derive(Clone)]
pub enum RenderMode {
    Application,
    Component,
}

pub struct Params {
    pub mode: RenderMode,
    pub item_counter: std::cell::Cell<usize>,
    pub app_type: TokenStream,
    pub context: TokenStream,
    pub sup_apps_collection_name: Option<TokenStream>,
}

impl Params {
    pub fn new(mode: &RenderMode, options: &super::MacroOptions) -> Self {
        let app_type = options.app_type.as_ref().map_or_else(
            || match mode {
                RenderMode::Component => quote! { A },
                RenderMode::Application => quote! { Self },
            },
            |at| quote! { #at },
        );
        let context = options
            .context
            .as_ref()
            .map_or_else(|| quote! {context}, |c| quote! {#c});
        Self {
            mode: mode.clone(),
            item_counter: std::cell::Cell::new(0),
            app_type,
            context,
            sup_apps_collection_name: None,
        }
    }

    pub fn get_item_name(&self, name: &str) -> TokenStream {
        let count = self.item_counter.get();
        let name = format!("_{}_{}_", name, count);
        self.item_counter.set(count + 1);
        let ident = Ident::new(&name, proc_macro2::Span::call_site());
        quote! { #ident }
    }

    pub fn set_sup_apps_collection_name(&mut self, name: TokenStream) {
        self.sup_apps_collection_name = Some(name);
    }

    pub fn sup_apps_collection_name(&self) -> &TokenStream {
        self.sup_apps_collection_name
            .as_ref()
            .expect("Should only call this in `application!`")
    }
}
