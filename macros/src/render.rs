use proc_macro2::{TokenStream, TokenTree};
use syn::spanned::Spanned;
use syn::{Ident, LitStr, Token};

mod generate;
mod helper;
mod kw;
mod params;
mod parse;
#[cfg(test)]
mod test;

pub use self::params::*;

pub struct MacroOptions {
    pub debug: bool,
    pub app_type: Option<Ident>,
    pub context: Option<Ident>,
}

impl Default for MacroOptions {
    fn default() -> Self {
        Self {
            debug: false,
            app_type: None,
            context: None,
        }
    }
}

pub struct MacroInput {
    pub options: MacroOptions,
    pub node_list: NodeList,
}

pub struct NodeList {
    pub nodes: Vec<Node>,
}

pub enum Node {
    Element(Box<Element>),
    Expression(SimpleExpression),
    Literal(LitStr),
    For(Box<For>),
    If(If),
    Match(Match),
    Component(Component),
    ComponentPlaceholder(TokenStream),
}

pub struct Element {
    pub no_update: bool,
    pub tag: Ident,
    pub attributes: Option<ElementAttributes>,
    pub content: Option<ElementContent>,
}

pub struct ElementAttributes {
    pub no_update_attributes: Vec<ElementAttribute>,
    pub literal_classes: Vec<LitStr>,
    pub no_update_classes: Vec<ConditionalClass>,

    pub attributes: Vec<ElementAttribute>,
    pub classes: Vec<ConditionalClass>,

    pub no_update_events: Vec<ElementEvent>,
    pub events: Vec<ElementEvent>,
}

pub struct ElementAttribute {
    pub no_update: bool,
    pub optional: self::parse::OptionalSymbol,
    pub custom: bool, // hyphened-separated-series of ident
    pub name: TokenStream,
    pub value: TokenStream,
    pub value_is_string_literal: bool,
    pub attribute_type: ElementAttributeType,
}

pub enum ElementAttributeType {
    Unknown,
    String,
    Bool,
    Index,
}

pub struct ConditionalClass {
    pub no_update: bool,
    pub class: LitStr,
    pub condition: TokenStream,
}

pub struct ElementEvent {
    pub no_update: bool,
    pub event_name: Ident,
    pub handler: ElementEventHandler,
}

pub enum ElementEventHandler {
    Placeholder(TokenStream),
    EnumVariant(TokenStream),
    QuestionEnumVariant(TokenStream), // `(?)` will be dropped while parsing
}

pub struct SimpleExpression {
    pub no_update: bool,
    pub optional: self::parse::OptionalSymbol,
    pub value: TokenStream,
}

pub enum ElementContent {
    NodeList(NodeList),
    SubApp(SubApp),
    TabApps(TabApps),
}

pub struct SubApp {
    kw: kw::sub_app,
    app_type: TokenStream,
    id: TokenStream,
}

pub struct TabApps {
    kw: kw::tab_apps,
    match_value: TokenStream,
    arms: Vec<TabApp>,
}

pub struct TabApp {
    match_value: TokenStream,
    sub_app: SubApp,
}

pub struct For {
    no_update: bool,
    iter_expression: TokenStream,
    options: ForOptions,
    content: ForContent,
}

pub struct ForOptions {
    for_hint: Option<TokenStream>,
    for_setup: Option<TokenStream>,
    for_item_key: Option<TokenStream>,
}

pub enum ForContent {
    Element(Box<Element>),
    Expression(SimpleExpression),
}

pub struct If {
    pub no_update: bool,
    pub arms: Vec<IfArm>,
    pub final_else: Option<ArmBody>,
}

pub struct IfArm {
    pub condition: TokenStream,
    pub body: ArmBody,
}

pub struct ArmBody {
    pub node_list: NodeList,
}

pub struct Match {
    pub no_update: bool,
    pub match_value: TokenStream,
    pub arms: Vec<MatchArm>,
}

pub struct MatchArm {
    pub pattern: TokenStream,
    pub body: ArmBody,
}

pub struct Component {
    pub no_update: bool,
    pub component_type: Ident,
    pub regular_fields: Vec<ComponentRegularField>,
    pub events: Vec<ComponentEvent>,
    pub childs: Vec<ChildComponent>,
}

pub struct ComponentRegularField {
    pub id: Ident,
    pub value: TokenStream,
}

pub struct ComponentEvent {
    // pub no_update: bool, this is available in ElementEvent
    pub id: Ident,
    pub event: ElementEvent,
}

pub struct ChildComponent {
    pub id: Ident,
    pub comp: Component,
}

impl NodeList {
    pub fn inspect_no_update(&mut self) -> bool {
        // use `fold` here to make sure inspect no update must run on every node
        let no_update_count = self.nodes.iter_mut().fold(0, |count, n| {
            if n.inspect_no_update() {
                count + 1
            } else {
                count
            }
        });
        no_update_count == self.nodes.len()
    }

    pub fn inspect_for_content(&self) {
        self.nodes.iter().for_each(Node::inspect_for_content);
    }
}

impl Node {
    fn inspect_no_update(&mut self) -> bool {
        match self {
            Node::Element(item) => item.inspect_no_update(),
            Node::Expression(item) => item.no_update,
            Node::Literal(_) => true,
            Node::For(item) => item.inspect_no_update(),
            Node::If(item) => item.inspect_no_update(),
            Node::Match(item) => item.inspect_no_update(),
            Node::Component(item) => item.no_update,
            Node::ComponentPlaceholder(_) => false,
        }
    }

    pub fn inspect_for_content(&self) {
        match self {
            Node::Element(item) => item.inspect_for_content(),
            Node::Expression(_) => {}
            Node::Literal(_) => {}
            Node::For(item) => item.inspect_for_content(),
            Node::If(item) => item.inspect_for_content(),
            Node::Match(item) => item.inspect_for_content(),
            Node::Component(_) => {}
            Node::ComponentPlaceholder(_) => {}
        }
    }
}

impl Element {
    fn inspect_no_update(&mut self) -> bool {
        if self.no_update {
            return true;
        }

        let no_update = if let Some(c) = &mut self.content {
            c.inspect_no_update()
        } else {
            true
        };
        if let Some(a) = &self.attributes {
            self.no_update = no_update && a.inspect_no_update();
        } else {
            self.no_update = no_update;
        }
        self.no_update
    }

    fn validate_element_attribute(&mut self) {
        let tag = self.tag.to_string();
        if let Some(ref mut a) = self.attributes {
            a.validate_element_attribute(&tag);
        }
    }

    pub fn inspect_for_content(&self) {
        if let Some(content) = &self.content {
            content.inspect_for_content();
        }
    }
}

impl ElementAttributes {
    fn inspect_no_update(&self) -> bool {
        self.attributes.is_empty() && self.classes.is_empty() && self.events.is_empty()
    }

    fn validate_element_attribute(&mut self, tag: &str) {
        self.attributes
            .iter_mut()
            .for_each(|a| a.validate_element_attribute(tag));
        self.no_update_attributes
            .iter_mut()
            .for_each(|a| a.validate_element_attribute(tag));
    }
}

impl ElementAttribute {
    #[cfg(not(test))]
    fn validate_optional(&self, tag: &str) {
        if let Some(question) = self.optional {
            if !helper::is_allow_optional(tag, &self.name.to_string()) {
                question
                    .span()
                    .unstable()
                    .error(format!(
                        "Attribute `{}` of element `{}` does not support optional value",
                        self.name, tag
                    ))
                    .emit();
            }
        }
    }
    #[cfg(not(test))]
    fn validate_element_attribute(&mut self, tag: &str) {
        if self.custom {
            self.attribute_type = ElementAttributeType::String;
            return;
        }
        if let Some(at) = helper::get_attribute_type(tag, &self.name.to_string()) {
            self.attribute_type = at;
            self.validate_optional(tag);
        } else {
            let mut ts = self.name.clone().into_iter();
            let name = ts.next().expect("There must be a token for attribute name");
            if let TokenTree::Ident(ident) = &name {
                ident
                    .span()
                    .unstable()
                    .error(format!(
                        "Does html tag `{}` really have an attribute named `{}`? Yes? Please file an issue, thank you!",
                        tag,
                        name
                    ))
                    .emit();
            }
        }
    }

    #[cfg(test)]
    fn validate_element_attribute(&mut self, tag: &str) {
        if self.custom {
            self.attribute_type = ElementAttributeType::String;
            return;
        }
        if let Some(at) = helper::get_attribute_type(tag, &self.name.to_string()) {
            self.attribute_type = at;
            if self.optional.is_some() {
                if !helper::is_allow_optional(tag, &self.name.to_string()) {
                    panic!(format!(
                        "Attribute `{}` of element `{}` does not support optional value",
                        self.name, tag
                    ));
                }
            }
        } else {
            panic!(format!(
                "Does html tag `{}` really have an attribute named `{}`? Yes? Please file an issue, thank you!",
                tag, self.name
            ));
        }
    }

    fn name_string(&self) -> String {
        if self.custom {
            self.name.to_string().replace(" ", "")
        } else {
            self.name.to_string()
        }
    }
}

impl ElementContent {
    fn inspect_no_update(&mut self) -> bool {
        match self {
            ElementContent::NodeList(item) => item.inspect_no_update(),
            ElementContent::SubApp(_) => false,
            ElementContent::TabApps(_) => false,
        }
    }
    fn child_node_count(&self) -> usize {
        match self {
            ElementContent::NodeList(item) => item.nodes.len(),
            ElementContent::SubApp(_) => 1,
            ElementContent::TabApps(_) => 1,
        }
    }
    pub fn inspect_for_content(&self) {
        match self {
            ElementContent::NodeList(item) => item.inspect_for_content(),
            ElementContent::SubApp(item) => item.inspect_for_content(),
            ElementContent::TabApps(item) => item.inspect_for_content(),
        }
    }
}

impl SubApp {
    pub fn inspect_for_content(&self) {
        self.kw
            .span()
            .unstable()
            .error("Simi does not support a sub app inside a for-loop")
            .emit();
    }
}

impl TabApps {
    pub fn inspect_for_content(&self) {
        self.kw
            .span()
            .unstable()
            .error("Simi does not support a sub app inside a for-loop")
            .emit();
    }
}

impl For {
    fn inspect_no_update(&mut self) -> bool {
        if self.no_update {
            return true;
        }
        match self.content {
            ForContent::Element(ref mut e) => {
                e.inspect_no_update();
            }
            ForContent::Expression(ref mut _e) => {}
        }
        // But the `For` must alway need update unless user prefix it with `#`
        self.no_update
    }

    pub fn inspect_for_content(&self) {
        match &self.content {
            ForContent::Element(e) => e.inspect_for_content(),
            ForContent::Expression(_) => {}
        }
    }
}

impl If {
    fn inspect_no_update(&mut self) -> bool {
        if self.no_update {
            return true;
        }
        // Run inspect no update for each arm
        self.arms.iter_mut().for_each(|a| {
            a.body.node_list.inspect_no_update();
        });
        if let Some(ref mut final_else) = self.final_else {
            final_else.node_list.inspect_no_update();
        }
        self.no_update
    }

    pub fn inspect_for_content(&self) {
        self.arms
            .iter()
            .for_each(|a| a.body.node_list.inspect_for_content());
        if let Some(final_else) = &self.final_else {
            final_else.node_list.inspect_for_content();
        }
    }
}

impl Match {
    fn inspect_no_update(&mut self) -> bool {
        if self.no_update {
            return true;
        }
        // Run inspect no update for each arm
        self.arms.iter_mut().for_each(|a| {
            a.body.node_list.inspect_no_update();
        });
        self.no_update
    }

    pub fn inspect_for_content(&self) {
        self.arms
            .iter()
            .for_each(|a| a.body.node_list.inspect_for_content());
    }
}
