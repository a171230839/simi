#![feature(proc_macro_hygiene)]
#![feature(exclusive_range_pattern)]
#![cfg(target_arch = "wasm32")]
use simi::prelude::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

#[simi_app]
struct TestApp {
    year: u32,
    month: u32,
}

enum Msg {
    MonthUp,
    MonthDown,
    YearUp,
    YearDown,
}

impl Application for TestApp {
    type Message = Msg;
    type Context = ();
    type Parent = ();
    fn init() -> Self {
        Self {
            year: 2018,
            month: 11,
        }
    }
    fn update(&mut self, m: Self::Message, _main: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::MonthDown => {
                self.month -= 1;
                if self.month < 1 {
                    self.month = 12;
                    self.year -= 1
                }
            }
            Msg::MonthUp => {
                self.month += 1;
                if self.month > 12 {
                    self.month = 1;
                    self.year += 1
                }
            }
            Msg::YearUp => self.year += 1,
            Msg::YearDown => self.year -= 1,
        }
        true
    }
    fn render(&self, context: RenderContext<Self>) {
        application! {
            //@debug
            div (id="match") {
                "Rust wasm: "
                match self.year {
                    2006..2018 => "available, but not ready yet. `Domafic` needs `Emscripten`, while `Yew` primarily builds on `Emscripten targetted Stdweb`",
                    2018 => {
                        match self.month {
                            1..7 => "yeah, wasm-bindgen is available. But still just `Yew` (on Stdweb), `Domafic` is inactive",
                            7 => "ideas about `Simi` come to its author head. He try to do some experiment with it. On 2018-07-19, he decided to register a placeholder on crates.io",
                            8..11 => "Simi 0.1.x. And here come with other frameworks too: `percy`, `ruukh`, `draco`, `squark`",
                            11 => "Simi 0.2.0 is rewriting from scratch. And here is the test for `if`/`match`, writting on 2018-11-12",
                            _ => "Simi 0.2.0 may probably available on crates.io now! I am trying to make it",
                        }
                    }
                    2019..3000 => "yeah, Rust wasm may be very blooming now! Will `Simi` still active? I hope it survive",
                    _ => "uh... oh, who are you? Is Rust available in your era???"
                }
                // This final `dot` to make sure everything must be insert before this
                "."
            }
        }
    }
}

#[wasm_bindgen_test]
fn simple_match() {
    let main: RcMain<TestApp> = simi_test::start_app();
    let div = simi_test::Element::id("match");
    assert_eq!(
        "Rust wasm: Simi 0.2.0 is rewriting from scratch. And here is the test for `if`/`match`, writting on 2018-11-12.",
        div.as_node().text_content().expect("div content")
    );
    main.send_message(Msg::YearDown);
    assert_eq!(
        "Rust wasm: available, but not ready yet. `Domafic` needs `Emscripten`, while `Yew` primarily builds on `Emscripten targetted Stdweb`.",
        div.as_node().text_content().expect("div content")
    );
    main.send_message(Msg::YearUp);
    assert_eq!(
        "Rust wasm: Simi 0.2.0 is rewriting from scratch. And here is the test for `if`/`match`, writting on 2018-11-12.",
        div.as_node().text_content().expect("div content")
    );
    main.send_message(Msg::MonthUp);
    assert_eq!(
        "Rust wasm: Simi 0.2.0 may probably available on crates.io now! I am trying to make it.",
        div.as_node().text_content().expect("div content")
    );
    main.send_message(Msg::MonthDown);
    main.send_message(Msg::MonthDown);
    assert_eq!(
        "Rust wasm: Simi 0.1.x. And here come with other frameworks too: `percy`, `ruukh`, `draco`, `squark`.",
        div.as_node().text_content().expect("div content")
    );
    main.send_message(Msg::MonthDown);
    main.send_message(Msg::MonthDown);
    main.send_message(Msg::MonthDown);
    assert_eq!(
        "Rust wasm: ideas about `Simi` come to its author head. He try to do some experiment with it. On 2018-07-19, he decided to register a placeholder on crates.io.",
        div.as_node().text_content().expect("div content")
    );
    main.send_message(Msg::MonthDown);
    assert_eq!(
        "Rust wasm: yeah, wasm-bindgen is available. But still just `Yew` (on Stdweb), `Domafic` is inactive.",
        div.as_node().text_content().expect("div content")
    );
    main.send_message(Msg::YearUp);
    assert_eq!(
        "Rust wasm: yeah, Rust wasm may be very blooming now! Will `Simi` still active? I hope it survive.",
        div.as_node().text_content().expect("div content")
    );
}
