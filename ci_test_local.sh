#!/bin/bash

# Run this locally to make sure all tests pass and all examples build

set -e

# Test simi-macros
echo "Run tests for 'simi-macros'"
cd ./macros
cargo +nightly test

echo "Clippying on 'simi-macros'"
cargo +nightly clippy -- -D warnings

# Test simi
cd ..
./build_all_examples.sh

echo "Clippying on 'simi'"
cargo +nightly clippy -- -D warnings

echo "Run all ./tests"
simi test
