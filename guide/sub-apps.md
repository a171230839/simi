# Sub apps
> No doc available yet, but you can look in `examples/sub-app`

# Tab apps
If you want multi sub apps run in a tab (only one app is active):
```rust
enum TabMode {
    Tab1,
    Tab2
}

#[simi_app]
pub struct TabApp {
    mode: TabMode,
}

impl Application for CwmApp {
    ....
    ....
    fn render(&self, context: RenderContext<Self>) {
        application! {
            div {
                @tab_apps self.mode {
                    TabMode::Tab1 {
                        @sub_app(1, path::to::SubApp1)
                    }
                    TabMode::Tab2 {
                        @sub_app(2, path::to::SubApp2)
                    }
                }
            }
        }
    }
}
```
