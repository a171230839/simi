//! Handle errors

use wasm_bindgen::JsValue;

/// Use log::error! to log the message if `rs` is an Err
pub fn log_result_error<R>(rs: Result<R, JsValue>) {
    if let Err(e) = rs {
        log::error!(
            "{}",
            e.as_string()
                .unwrap_or_else(|| "(No error message?)".to_string())
        )
    }
}

/// An error string report by simi
#[derive(Debug)]
pub struct ErrorString(String);

impl ::std::fmt::Display for ErrorString {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "{}", self.0)
    }
}

impl ErrorString {
    /// New ErrorString
    pub fn new(message: String) -> Self {
        ErrorString(message)
    }
}

impl Into<String> for ErrorString {
    fn into(self) -> String {
        self.0
    }
}

impl From<&str> for ErrorString {
    fn from(message: &str) -> Self {
        ErrorString(message.to_string())
    }
}

impl From<JsValue> for ErrorString {
    fn from(value: JsValue) -> ErrorString {
        ErrorString(
            value
                .as_string()
                .map(|s| s.to_string())
                .unwrap_or_else(|| "No message in JsValue for the error".to_string()),
        )
    }
}

#[cfg(feature = "fetch_json")]
impl From<::serde_json::Error> for ErrorString {
    fn from(e: ::serde_json::Error) -> ErrorString {
        ErrorString(format!("Serde error: {}", e))
    }
}
